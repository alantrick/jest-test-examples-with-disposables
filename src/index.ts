const ERROR_LEVELS = ["critical", "error"] as const;
const INFO_LEVELS = ["error", "info", "debug"] as const;
const LOG_LEVELS = [...ERROR_LEVELS, ...INFO_LEVELS] as const;
type LogLevel = (typeof LOG_LEVELS)[number];

function isLogLevel(input: string | undefined): input is LogLevel {
  return input !== undefined && LOG_LEVELS.includes(input as LogLevel);
}

export function log(message: string, level?: LogLevel) {
  const minLevel = process.env.LOG_LEVEL ?? "info";
  const logLevel = level ?? "info";
  if (!isLogLevel(minLevel)) {
    throw new Error(`Unrecognized LOG_LEVEL "${minLevel}"`);
  }
  if (LOG_LEVELS.indexOf(minLevel) < LOG_LEVELS.indexOf(logLevel)) {
    return;
  }
  const fn = ERROR_LEVELS.includes(logLevel as any)
    ? console.error
    : console.log;
  const now = new Date();
  fn(`${now.toISOString()} ${logLevel.toUpperCase()}: ${message}`);
}
