import disposableEnv from "disposable-env";
import disposableSpyOn from "disposable-spy-on";
import disposableMockDate from "disposable-mock-date";
import { disposableCollection } from "disposable-collections";

import { log } from "../src";

describe("forced checks with disposables", () => {
  type MockProps = {
    LOG_LEVEL?: string;
    expectConsoleLog?: string;
    expectConsoleError?: string;
  };

  const makeMocks = (props: MockProps) => {
    const env = disposableEnv({ LOG_LEVEL: props.LOG_LEVEL });
    const date = disposableMockDate("2020-02-02");
    const consoleLog = disposableSpyOn(console, "log").mockReturnValue();
    const consoleError = disposableSpyOn(console, "error").mockReturnValue();
    const defer = new DisposableStack();
    defer.defer(() => {
      if (props.expectConsoleLog !== undefined) {
        expect(consoleLog).toBeCalledTimes(1);
        expect(consoleLog).toBeCalledWith(props.expectConsoleLog);
      } else {
        expect(consoleLog).toBeCalledTimes(0);
      }
      if (props.expectConsoleError !== undefined) {
        expect(consoleError).toBeCalledTimes(1);
        expect(consoleError).toBeCalledWith(props.expectConsoleError);
      } else {
        expect(consoleError).toBeCalledTimes(0);
      }
    });
    return disposableCollection([
      env,
      date,
      // note: defer has to be before the spies so it is run first
      defer,
      consoleLog,
      consoleError,
    ]);
  };

  it("errors with bad LOG_LEVEL env", () => {
    using _ = makeMocks({ LOG_LEVEL: "bad" });
    expect(() => log("foo")).toThrow(Error('Unrecognized LOG_LEVEL "bad"'));
  });

  it("defaults to the info level", () => {
    using _ = makeMocks({
      expectConsoleLog: "2020-02-02T00:00:00.000Z INFO: foo",
    });
    log("foo");
  });

  it("does not log when LOG_LEVEL is below level", () => {
    using _ = makeMocks({ LOG_LEVEL: "error" });
    log("foo", "info");
  });

  it("writes to console.error when level is critical", () => {
    using _ = makeMocks({
      expectConsoleError: "2020-02-02T00:00:00.000Z CRITICAL: foo",
    });
    log("foo", "critical");
  });
});
