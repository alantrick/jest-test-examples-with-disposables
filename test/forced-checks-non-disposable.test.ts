import mockDate from "mockdate";
import { log } from "../src";

describe("forced checks without disposables", () => {
  let expectConsoleLog: string | undefined;
  let expectConsoleError: string | undefined;
  const consoleLog = jest.spyOn(console, "log").mockReturnValue();
  const consoleError = jest.spyOn(console, "error").mockReturnValue();

  beforeEach(() => {
    mockDate.set("2020-02-02");
  });

  afterEach(() => {
    if (expectConsoleLog !== undefined) {
      expect(consoleLog).toBeCalledTimes(1);
      expect(consoleLog).toBeCalledWith(expectConsoleLog);
    } else {
      expect(consoleLog).toBeCalledTimes(0);
    }
    if (expectConsoleError !== undefined) {
      expect(consoleError).toBeCalledTimes(1);
      expect(consoleError).toBeCalledWith(expectConsoleError);
    } else {
      expect(consoleError).toBeCalledTimes(0);
    }
    expectConsoleLog = undefined;
    expectConsoleError = undefined;
    process.env = {};
    jest.clearAllMocks();
    mockDate.reset();
  });

  it("errors with bad LOG_LEVEL env", () => {
    process.env = { LOG_LEVEL: "bad" };
    expect(() => log("foo")).toThrow(Error('Unrecognized LOG_LEVEL "bad"'));
  });

  it("defaults to the info level", () => {
    expectConsoleLog = "2020-02-02T00:00:00.000Z INFO: foo";
    log("foo");
  });

  it("does not log when LOG_LEVEL is below level", () => {
    process.env = { LOG_LEVEL: "error" };
    log("foo", "info");
  });

  it("writes to console.error when level is critical", () => {
    expectConsoleError = "2020-02-02T00:00:00.000Z CRITICAL: foo";
    log("foo", "critical");
  });
});
