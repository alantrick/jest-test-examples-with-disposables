import disposableEnv from "disposable-env";
import disposableSpyOn from "disposable-spy-on";
import disposableMockDate from "disposable-mock-date";

import { log } from "../src";

describe("tests with separate mocks", () => {
  it("errors with bad LOG_LEVEL env", () => {
    using _d = disposableMockDate("2020-02-02");
    using _e = disposableEnv({ LOG_LEVEL: "bad" });
    expect(() => log("foo")).toThrow(Error('Unrecognized LOG_LEVEL "bad"'));
  });

  it("defaults to the info level", () => {
    using _ = disposableMockDate("2020-02-02");
    using spy = disposableSpyOn(console, "log").mockReturnValue();
    log("foo");
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith("2020-02-02T00:00:00.000Z INFO: foo");
  });

  it("does not log when LOG_LEVEL is below level", () => {
    using _d = disposableMockDate("2020-02-02");
    using _e = disposableEnv({ LOG_LEVEL: "error" });
    using spyLog = disposableSpyOn(console, "log").mockReturnValue();
    using spyError = disposableSpyOn(console, "error").mockReturnValue();
    log("foo", "info");
    expect(spyLog).toBeCalledTimes(0);
    expect(spyError).toBeCalledTimes(0);
  });

  it("writes to console.error when level is critical", () => {
    using _ = disposableMockDate("2020-02-02");
    using spy = disposableSpyOn(console, "error").mockReturnValue();
    log("foo", "critical");
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith("2020-02-02T00:00:00.000Z CRITICAL: foo");
  });
});
