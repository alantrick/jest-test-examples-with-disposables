import mockDate from "mockdate";
import { log } from "../src";

describe("testing without disposables", () => {
  beforeEach(() => {
    mockDate.set("2020-02-02");
  });

  afterEach(() => {
    process.env = {};
    jest.clearAllMocks();
    mockDate.reset();
  });

  it("errors with bad LOG_LEVEL env", () => {
    process.env = { LOG_LEVEL: "bad" };
    expect(() => log("foo")).toThrow(Error('Unrecognized LOG_LEVEL "bad"'));
  });

  it("defaults to the info level", () => {
    const spy = jest.spyOn(console, "log").mockReturnValue();
    log("foo");
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith("2020-02-02T00:00:00.000Z INFO: foo");
  });

  it("does not log when LOG_LEVEL is below level", () => {
    process.env = { LOG_LEVEL: "error" };
    const spyLog = jest.spyOn(console, "log").mockReturnValue();
    const spyError = jest.spyOn(console, "error").mockReturnValue();
    log("foo", "info");
    expect(spyLog).toBeCalledTimes(0);
    expect(spyError).toBeCalledTimes(0);
  });

  it("writes to console.error when level is critical", () => {
    const spy = jest.spyOn(console, "error").mockReturnValue();
    log("foo", "critical");
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith("2020-02-02T00:00:00.000Z CRITICAL: foo");
  });
});
