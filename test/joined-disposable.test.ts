import disposableEnv from "disposable-env";
import disposableSpyOn from "disposable-spy-on";
import disposableMockDate from "disposable-mock-date";
import { disposableObject } from 'disposable-collections';

import { log } from "../src";

describe("tests with joined mocks", () => {
  const makeMocks = ({ LOG_LEVEL }: { LOG_LEVEL?: string | undefined }) =>
    disposableObject({
      env: disposableEnv({ LOG_LEVEL }),
      date: disposableMockDate("2020-02-02"),
      consoleLog: disposableSpyOn(console, "log").mockReturnValue(),
      consoleError: disposableSpyOn(console, "error").mockReturnValue(),
    });


  it("errors with bad LOG_LEVEL env", () => {
    using mocks = makeMocks({ LOG_LEVEL: "bad" });
    expect(() => log("foo")).toThrow(Error('Unrecognized LOG_LEVEL "bad"'));
    expect(mocks.consoleLog).toBeCalledTimes(0);
    expect(mocks.consoleError).toBeCalledTimes(0);
  });

  it("defaults to the info level", () => {
    using mocks = makeMocks({})
    log("foo");
    expect(mocks.consoleLog).toBeCalledTimes(1);
    expect(mocks.consoleLog).toBeCalledWith("2020-02-02T00:00:00.000Z INFO: foo");
    expect(mocks.consoleError).toBeCalledTimes(0);
  });

  it("does not log when LOG_LEVEL is below level", () => {
    using mocks = makeMocks({ LOG_LEVEL: "error" });
    log("foo", "info");
    expect(mocks.consoleLog).toBeCalledTimes(0);
    expect(mocks.consoleError).toBeCalledTimes(0);
  });

  it("writes to console.error when level is critical", () => {
    using mocks = makeMocks({})
    log("foo", "critical");
    expect(mocks.consoleError).toBeCalledTimes(1);
    expect(mocks.consoleError).toBeCalledWith("2020-02-02T00:00:00.000Z CRITICAL: foo");
    expect(mocks.consoleLog).toBeCalledTimes(0);
  });
});

